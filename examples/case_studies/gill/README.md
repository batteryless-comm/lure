# GILL Case Study
This example configuration produces results evaluating the GILL (Good-enough ILL) intermittent link-layer protocol. To run the experiments, invoke Lure as a module:

    python -m lure

To plot the results, run plot.py:

    python plot.py

The plots will be placed in `output/figures/`.
