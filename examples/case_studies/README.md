# Case Studies
This directory contains the case studies from [insert publication here]. See the README in each subdirectory for instructions on reproducing the case study results.