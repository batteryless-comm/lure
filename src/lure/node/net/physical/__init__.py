from lure.node.net.physical.physical import Physical
from lure.node.net.physical.all_nodes import AllNodesPhysical
from lure.node.net.physical.cartesian_physical import CartesianPhysical
from lure.node.net.physical.manual_physical import ManualPhysical