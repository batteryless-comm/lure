# Interactive Time Series Plot example
This example runs a simple experiment and opens an interactive, scrollable plot showing some detailed communication data. You must be a system with a GUI to view this plot.

To run the experiment and open the plot, use:

    python run.py