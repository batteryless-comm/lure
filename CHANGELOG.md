# Release
All notable changes to the repository and the releases are documented in this file for reference.
## Overview

* [v0.1](#v0.1)
* [v1.0](#v1.0)
* [Template](#template)
* [License](#license)

## <a name="v0.1"></a> v0.1

* Added a README_pre_release.md file documenting the project overview to provide a permanent repo link for Lure Perforamance Evaluation submission [README_pre_release](README_pre_release).

## <a name="v1.0"></a> v1.0

### Added
* Added all of Lure simulator files after Performance Evaluation Journal acceptance decision.

## <a name="template"></a> Template

### Added
* New Software files

### Changed
* Changes to existing repository by adding new files

### Deprecated
* No files removed

### Removed
* No files removed

### Fixed
* No Fixes

### Security
* No changes

# License
BSD 3-Clause License

Copyright (c) 2024 The Lure Authors

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.