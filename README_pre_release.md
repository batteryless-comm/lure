# Lure
This is the future home of the public repository for the Lure batteryless intermittent sensor network simulator. The code for Lure will be committed here once the journal submission for Lure has been published.



# How to cite this

10.5281/zenodo.11062223 - permanent DOI