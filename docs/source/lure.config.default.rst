lure.config.default package
===========================

Module contents
---------------

.. automodule:: lure.config.default
   :members:
   :undoc-members:
   :show-inheritance:
