from lure.lure import Lure
from lure.plotter import Plotter
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)
from matplotlib import rc
from lure.node.stats import StatType



# Example of a mac_state interactive plot
# Setup Lure
simulator = Lure(config_dir='config', top_config_file='lure.json', output_dir='output', resume=True)
results = simulator.run()
data = simulator.load_results(output_dir ='output')
'''
    Instructions:
    1) Set what stats you want to plot. 0th index is the top of the stack of subplots.
    2) Use the plot_time_series_interactive() function. Giving the stat name and which nodes from your experiment you want to track (by node_id).
    3) The -/+ Range buttons decrease/increase the range of the x-axis by 25 ms
    4) The L/R Buttons shift the window of the plot left and right by 10 ms
    5) The save button saves the current window to a pdf in Plotter's output directory
'''
# Interactive Plot
plotter = Plotter(trials=1, results=data, output_dir='output/figures/time_series/', extension='pdf')
stats = [StatType.MAC_IS_SENDING,StatType.MAC_IS_TRANSMITTING, StatType.MAC_IS_LISTENING, StatType.MAC_IS_RECEIVING]
nodes = [0,1]
plotter.plot_time_series_interactive(names=stats, series_metadatakey='2_2', xval=0.2, node_ids=nodes)
