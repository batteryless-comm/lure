.. Lure documentation master file, created by
   sphinx-quickstart on Sun Apr 16 15:47:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lure's documentation!
================================

.. toctree::
   :caption: Contents:
   
   readme_link
   modules




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
